+++
title = "Definindo os candidados"
date = 2022-08-07
updated = 2022-08-08

[extra]
reading_time = true
+++

Aqui será mostrada a formatação dos campos dos **candidatos** na página da WEB URNA.

<!-- more -->

**Atenção**, o campo é de livre edição e uma formatação errada irá impossibilitar seu uso!!!

Tentamos seguir a numeração informada pelo TSE, confira [clicando aqui](https://www.tse.jus.br/eleicoes/processo-eleitoral-brasileiro/candidaturas/identificacao-numerica-dos-candidatos-que-concorrem-as-eleicoes), e limitamos os cargos eletivos por conta da falta de recursos para desenvolver este projeto.

<div class="is-fullwidth has-text-centered"><img src="/candidatos.png"></div>

Como pode ser observado na imagem acima, os campos de edição dos candidados são parecidos com os dos eleitores, ou seja, na WEB URNA iremos também usar um formato explicado para os eleitores de **número** + **separador** + **nome completo do eleitor**, onde:

- **número** seria o equivalente ao título do eleitor, que padronizamos conforme [TSE determinou](https://www.tse.jus.br/eleicoes/processo-eleitoral-brasileiro/candidaturas/identificacao-numerica-dos-candidatos-que-concorrem-as-eleicoes) (não testamos, mas deve funcionar com mais ou menos dígitos).
- **separador** seria apenas [` - `], ou seja, 1 espaço simples, um hífen e mais 1 espaço simples (este será o separador dos candidatos também).
- **nome completo do eleitor** seria o nome, fictício ou não, do eleitor (campo sem importância prática).

**Importante:** não deixe linhas em branco em momento algum, o software não está tratando formatação errada!
+++
title = "Alterando os eleitores"
date = 2022-08-07
updated = 2022-08-08

[extra]
reading_time = true
+++

Aqui será mostrada a formatação do campo **eleitores** na página da WEB URNA.

<!-- more -->

**Atenção**, o campo é de livre edição e uma formatação errada irá impossibilitar seu uso!!!

<div class="is-fullwidth has-text-centered"><img src="/eleitores.png"></div>

Como pode ser observado na imagem acima do campo de edição dos eleitores, na WEB URNA iremos usar um formato simples de **número** + **separador** + **nome completo do eleitor**, onde:

- **número** seria o equivalente ao título do eleitor, que padronizamos com 6 dígitos (não testamos, mas deve funcionar com mais ou menos dígitos).
- **separador** seria apenas [` - `], ou seja, 1 espaço simples, um hífen e mais 1 espaço simples (este será o separador dos candidatos também).
- **nome completo do eleitor** seria o nome, fictício ou não, do eleitor (campo sem importância prática).

**Importante:** não deixe linhas em branco em momento algum, o software não está tratando formatação errada!
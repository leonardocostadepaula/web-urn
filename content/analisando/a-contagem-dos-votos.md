+++
title = "A contagem dos votos"
date = 2022-08-07
updated = 2022-08-10

[extra]
reading_time = true
+++
Penso, logo existo! Certo? Pensar e opinar ainda pode aqui no Brasil? Ai que medo...

O TSE publicou [este arquigo](https://www.tse.jus.br/comunicacao/noticias/2021/Julho/fato-ou-boato-e-falso-que-a-apuracao-das-eleicoes-seja-feita-de-forma-secreta-por-servidores-do-tse), que tal refletirmos sobre o assunto?

<!-- more -->

Antes de mais nada, trabalhei como mesário durante os testes da urna eletrônica (final do século passado), em 2 eleições na cidade de Curitiba. Neta época estavam sendo testadas as primeiras versões da urna eletrônica, gosto de ressaltar que: **naquela época as urnas eletrônicas imprimiam o voto**, votos estes que eram armazenados em malotes opacos para futura conferência, se houvesse necessidade!

Lembro-me da matéria do jornal falando que "algumas poucas pessoas privilegiadas fazem a apuração dos votos", só que não mesmo!

A matéria é mentirosa mesmo, por isso o tribunal a rotulou de ***FAKE NEWS***!
Ela é mentirosa porque desde a implementação da urna eletrônica os votos são contados automaticamente pela máquina, conforme sua programação! Dê uma olha nesta [matéria do G1](https://g1.globo.com/politica/noticia/2015/11/auditoria-do-psdb-nao-encontra-fraudes-no-2-turno-das-eleicoes-2014.html), então se o presidente Bolsonaro falou que houve fraude em 2014 ele também fez ***FAKE NEWS***, afinal o que se descobriu é que não há como auditar os votos, o que é bem diferente de auditar a urna ou o seu programa!

Na minha opinião, receber os **arquivos das urnas eletrônicas** para conferir os votos serve apenas para verificar se não houve fraude fora da urna! Pois a garantia ainda está no código da urna que, diferente da WEB URNA, não é aberto [*opensource*](https://www.tecmundo.com.br/software/215130-open-source-funciona.htm))!

**Notas:**
- Muitos pensam que *opensource* é inseguro por ser aberto, já que os "bandidos" terão o código para estudar. Ser *opensource* não é igual ser inseguro!
- Lembre-se que 70% da WEB é *opensource* e nem por isso todos os servidores linux são invadidos.
- [Neste artigo](https://www.tecmundo.com.br/software/215130-open-source-funciona.htm) é explicado porque o *opensource* é mais seguro e tudo mais sobre o assunto.

Já sei, está pensando que o código é auditado e o programa é criptografado e que isso traz segurança para a urna, certo? Então vamos lá...
### Sobre a auditoria do código fonte

[O relatório completo da UNICAMP](https://www.ic.unicamp.br/~tomasz/misc/rel_final_site_TSE.pdf), que auditou a urna eletrônica deste ano, traz pontos importantes que o TSE não gota de divulgar, mas eu gosto!!!

#### Item 4.3 do relatório, consta: 

>"Mesmo havendo, por parte do TSE, o acompanhamento do desenvolvimento do software da UE pela empresa contratada, os detalhes de programação que implementam as funções requeridas, tanto nas novas urnas a fabricar como nas já  existentes, ficam a cargo da equipe da empresa."

Então vale ressaltar que há uma empresa contratada que faz tudo, o TSE só testa. Minha conclusão é que a garantia de que a urna já venha com um código malicioso pronto e oculto só pode ser quebrada com o código fonte aberto e com **todas** as urnas passando por testes de *hash* do seu programa. Sei, fui muito técnico agora, mas fazer o que, é um assunto bastante técnico.

#### Item 4.3 também traz:

> "Numa fase final, os programas passam à sessão de apresentação aos partidos durante um período de tempo determinado. Nesta sessão todos os arquivos que constituem o software da UE são gravados em um meio não volátil (CD-ROM), o qual é lacrado e assinado por todos os presentes à sessão de apresentação. Após este período, não há mecanismos simples e eficazes que permitam que representantes de algum partido, em qualquer lugar do país, possam confirmar que os programas usados na UE correspondem fielmente aos mesmos que foram lacrados e guardados no TSE, exceto através de uma auditoria."

##### Como é? "**Não há um mecanismo eficaz**"? Essa é minha opinião, se não há como comprovar que o código auditado é o usado nas urnas em todo o Brasil, como posso afirmar que ela foi aditada?

Você poderá continuar a leitura do relatório e notará que a auditoria, tanto enfatizada pelos supremos, não garante muita coisa não! É pura propaganda da urna e muito pouco vália (basta ler o capítulo 4 do relatório da UNICAMP, dê ênfase aos itens 4.3 e 4.4, que tratam do código fonte, pois todo o resto não é tão importante se o programa faz o que não se espera).

#### Item 4.4, olha o absurdo:

> "Pelo exame do código-fonte dos programas da UE, constata-se que a biblioteca de criptografia é utilizada somente no fim da eleição, momentos antes da impressão do BU, o que dá margem a alguma discussão sobre o impacto das operações realizadas pela mesma."

O que isso significa? Simples, que há um código não auditado que gera o B.U. e "*la garantia soy yo*", no caso a empresa contratada. Eu não confio meu voto neles, até mesmo porque já vi aqui no Brasil muita corrupção!

#### Item 4.5, o famoso *hash* que garante tudo:

>"Como o mecanismo de verificação de integridade e autenticidade dos arquivos está embutido dentro da própria UE, torna-se difícil criar um esquema totalmente seguro, já que os parâmetros de verificação estão contidos dentro da própria estrutura a ser verificada (neste caso, nos cartões de memória flash). Esta verificação poderia ser aprimorada com a adição de um mecanismo externo independente, o qual é objeto de recomendação deste relatório."

Mais uma afirmação dos supremos que se tornam apenas falácias, pois a própria urna é que verifica o seu *hash*, ao usarem a WEB URNA verão mais sobre isso! Os auditores concluem que o certo seria uma verificação do *hash* após a inseminação dos dados da seção (continue lendo para entender isso), lembrando que se o disquete contiver um *script* o mesmo é executado pela urna (uma grande piada para quem entende de programação).

#### Item 4.6, temos:

> "O fato da UE não se basear integralmente em um sistema operacional idêntico a um disponível no mercado pode gerar dúvidas quanto à segurança e não é uma prática recomendável."

Ou seja, para você entender melhor, a auditoria é sobre uma parte do código fonte, boa parte é fechada e proprietária.

##### Mas o TSE, por meio dos seus ministros, afirmam que a urna foi e sempre é auditada e que você está espalhando ***FAKE NEWS*** e pronto, vamos prender quem falar mau da urna! Já prenderam os professores da UNICAMP que escreveram este relatório?

#### Item 4.9, ainda diz:

>"Deve-se notar que o processo de inseminação é de difícil controle dada a grande quantidade de urnas, grande número de pessoas envolvidas e sua distribuição geográfica."

O relatório afirma que a inseminação (processo que alimentar cada urna com informações da sua seção eleitoral) pode ser garantia se **todos** os procedimento forem seguidos, mas ressalta a dificuldade disso acontecer. Os auditores também sugerem melhoria neste procedimento.

#### Item 4.10, cita um *script* (programa):

>"Entretanto, verificou-se que a utilização de um arquivo de script (.BAT), introduzido na UE via o disquete para cópia do arquivo de candidatos, não seria necessária uma vez que o script utilizado para inicialização da urna poderia ser alterado para incluir esta tarefa."

Traduzindo para os que não entenderam: o procedimento de inseminação para o segundo turno vem com um programinha a ser rodado pela urna, o que deixa a urna bastante frágil pois aceita rodar *scripts* (programas) via disquete!

#### Item 4.12, a cereja do bolo:

>"A desconfiança com relação à manipulação do BU pelo algoritmo de criptografia poderia ser minimizada com a simples inversão na ordem dos procedimentos finais, fazendo com que a impressão (divulgação) do BU ocorresse antes da chamada às rotinas de criptografia."

O que isso significa? É que o resultado apurado pela urna passa por uma criptografia, não auditada, que poderia, em hipótese remota de haver fraude no código não auditado da urna (aquele proprietário), antes de gerar o B.U., em outras palavras, o B.U. é gerado pela criptografia (código não auditado da empresa contratada) baseado nos votos registrado pela urna. Nos testes os auditores perceberam que tudo está perfeito, mas vale ressaltar que nada garante que o código auditado é o que estará na urna, conclusão dos auditores e minha, pois haverão alterações para melhoria da urna e não haverá outro processo de auditoria como este!

Claro que não há como comprovar fraudes nas urnas, mas de acordo com o relatório ela não foi auditada a contento, outro fato!

A verdade é que a urna brasileira hoje gera o RDV e o B.U. sem que haja transparência sobre o código fonte e ponto final! O que traz sim a possobilidade de questionarmos os resultados sem que sejamos presos por "ataque às instituições democráticas".

##### Democracia é o governo do povo, obviamente parece que alguns ministros não gostaram da escolha do povo na eleição de 2018.

No primeiro parágrado da conclusão (item 6) a equipe de auditores concluem que a urna é perfeita! Sério, isso mesmo, diferentemente da conclusão da equipe que auditou em 2013 ([relatório da UNICAMP de 2013](https://www.lasca.ic.unicamp.br/media/publications/relatorio-urna.pdf)), que escreveu isto na página 33 (idade de cristo):

>"O voto impresso distribui a auditoria do software entre todos os eleitores, que se tornam responsáveis por conferir que seus votos foram registrados corretamente pela urna eletrônica, desde que apuração posterior seja realizada para verificar que a contagem dos votos impressos corresponde exatamente à totalização eletrônica parcial."

Eu posso ter uma opinião diferente deles após ler o relatório? Posso, né? Não é crime isso, é?!

##### Por que tantas pessoas que antes defendiam o voto impresso hoje são contrários? O que mudou?

Leia o item 5 e 6 do relatório, com as recomendações e as conclusões dos auditores, em especial o 5.3, vamos mudar de assunto para o artigo não ficar muito grande!

### Mudando de assunto, vamos para a WEB URNA

A WEB URNA, aqui desenvolvida, funciona apenas offline e gera o RDV e o B.U. ([boletim de urna](https://www.tse.jus.br/comunicacao/noticias/2021/Junho/boletim-de-urna-traz-o-resultado-impresso-da-secao-de-votacao)), a questão é, ele gerou conforme os votos dos eleitores ou criou estes arquivos conforme um programa desconhecido?

Posso afirmar, por conta da minha experiência, que se houver um código inserido na urna após as auditorias do sistema, sim o B.U. pode ser comprometido! Isto é um fato e, por isso, a forma mais simples de conferir os votos relatado no B.U. não é conferindo o B.U., mas sim se houvesse um malote com os votos impressos de cada eleitor para um contagem manual (apenas se houvesse dúvida sobre o pleito e os resultado do B.U.).

Reflita agora sobre a citação:

>O sistema eletrônico é auditável antes, durante e depois da votação.

1. Sim, o sistema foi auditado antes, mas os relatórios apontos desconfiança e não comprovam que o que foi auditado é o que será usado no dia da eleição, além de a auditoria, segundo doutores da UNICAMP, ser insuficiente para asseguar o seu voto (não está escrito com estas palavras, mas qualquer pessoa com bom português pode concluir que é isso que diz o relatório).

2. Sim, há auditoria durente, algumas urnas são colocadas a prova, mas quem garante que não á uma forma de ativar ou não o moto **fraude** na inicialização da urna? Lembrando que os mesários fazem a inicialização da urna, pode haver mesário corrupto, fato!

3. Sim, há certa auditoria depois, mas muito ineficaz e que abrange uma parcela pequena de urnas, sem ansalisar a sua eficácia. Um voto impresso acabaria com todas essas dúvidas!

Em um artigo do TRE de SP, [link aqui](https://www.tre-sp.jus.br/comunicacao/noticias/2021/Julho/o-voto-eletronico-brasileiro-e-auditavel), o tribunal afirma que é auditável porque:
- Reimpressão do B.U.: tá, mas então *la garantia soy yo*!
- Recontagem via RDV e comparação com o B.U.: a urna gera o RDV, uma recontagem não ajudaria muito na confiabilidade, novamente *la garantia soy yo*!
- Verificação do *hash*: aqui tudo bem, se o programa que saiu do tribunal não já contiver o código malicioso, isso poderá pegar se o programa usado no dia da eleição foi o mesmo, mas essa verificação básica não é feita em 100% das urnas e nem da forma correta (falta gente interessada neste trabalho, porque seria necessário participação de fiscais de todos os interessados). **Portanto fica apenas na teoria**, além, é claro, de não garantir que um programador corrupto coloque um código malicioso antes de gerar o *hash*.
- Auditoria do código-fonte do cofre do TSE: ok, TSE, poderia me enviar o código fonte? A resposta é: **não**, por mais que juram ser uma forma de conferir, apenas poucas pessoas tem o acesso ao código fonte, se fosse aberto, o hash e a assinatura digital seriam confiáveis.
- Auditoria de funcionamento: legal, comprova mesmo! Entretanto, um código malicioso poderia prever que algumas urnas serão auditadas e viabilizar configuração na inicialização para rodar no **modo crackeado** ou não, bastando o mesário ser comprado (ninguém iria notar um botão apertado na inicialização da urna para ativar um modo ou outro).

Por fim, o TSE afirma que qualquer eleitor poderia participar das fiscalizações, isso não parece ser a pura verdade, porque eu li muitos documentos do TSE que citam quem pode e quais as condições para fazer parte do seleto grupo que terá acesso ao código fonte do sistema, infelizmente fiz esta pesquisa há 2 anos e não tenho a informação agora (quando tiver atualizo aqui). Mas, vejam esta notícias fresquinha que comprova que só poderá participar das fiscalizações quem eles (os supremos) desejarem:
- [Poder 360](https://www.poder360.com.br/eleicoes/tse-exclui-coronel-de-grupo-de-fiscalizacao-das-urnas/).
- [UOL Notícias](https://noticias.uol.com.br/colunas/leonardo-sakamoto/2022/08/08/fachin-remove-coronel-da-fiscalizacao-da-urna-por-agir-como-quinta-coluna.htm).
- [CNN Brasil](https://www.cnnbrasil.com.br/politica/tse-retira-de-grupo-de-fiscalizacao-coronel-do-exercito-que-teria-divulgado-fake-news/).
- [VEJA](https://veja.abril.com.br/coluna/radar/fachin-expulsa-coronel-do-exercito-de-equipe-da-defesa-que-fiscaliza-urnas/).

Então, e novamente o argumento é bastante fraco: ***FAKE NEWS***, mas o que separa ***FAKE NEWS*** de **OPNIÃO**?

### Termino com meu pensamento sobre votos auditáveis

><font size="4">"A impressão do voto não é o retorno da contagem manual dos votos, mas sim da simplificação da auditoria dos votos, pois com o voto em papel (impresso e verificado pelo eleitor) qualquer um poderá conferir se o B.U. gerado pela urna foi fraudado ou não, simples assim!"</font>
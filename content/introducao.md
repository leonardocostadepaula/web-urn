+++
title = "Bem-Vindo"
description = ""
template = "page.html"
date = 2022-08-08
updated = 2022-08-08

[extra]
reading_time = true
+++

Se chegou até aqui é porque está curioso sobre a segurança do seu voto e também interessa em saber mais sobre possibilidades de fraudes e tudo mais relacionados a uma **urna eletrônica**, certo?

Aqui você encontrará um local com indagações técnicas sobre diversas questões levantadas recentemente sobre as urnas eletrônicas brasileiras.

Muitas questões podem estar surgindo agora em sua mente, tais como: quem sou eu para falar sobre este tema; e o que este site pode trazer de novo. Se me der alguns minutos eu te conto tudo isso e muito mais, basta continuar a leitura deste artigo!

<!-- more -->

#### Quem sou eu para falar sobre este tema?
>Boa pergunta! Eu sou formado em eng. eletrônica por uma das melhores universidades do Brasil nesta área e tenho mestrado em sistemas embarcados, além de ser professor de sistemas microprocessados desde 2002 e atualmente sou professor da rede federal de educação.

#### O que me motivou?
>Para demonstrar, de forma prática ([WEB URNA](/urna/)), minhas teorias sobre os pontos fortes e fracos da urna eleitoral brasileira.

### Objetivo geral

><font size="4">Expor, publicamente, **minhas indagações** e **minhas opniões**, ambas técnicas, sobre a urna eletrônica brasileira.</font>

### Objetivos específicos

1. Estudar a legislação eleitoral e a **urna eletrônica**, para então expor aqui minhas indagações e opiniões.
1. **Implementar**, baseado no meu estudo, um sistema de **código aberto** ([WEB URNA](/urna/)) para comprovar, de forma prática, minhas opiniões sobre o que foi estudado.
1. **Indagar** e **opinar**, publicamente, notícias veiculadas sobre a **urna eletrônica**, inclusive do TSE e TREs.
1. Ajudar a população menos favorecida a distinguir **uma opinião** de **um fato**, podendo assim refletir sobre o que é ou não ***FAKE NEWS***!

Acredito que a urna eletrônica deveria ser *opensource* e que, de fato, qualquer pessoa pudesse auditá-la! Por isso a [WEB URNA](/urna/) e este site está totalmente aberto no [GitLab](https://gitlab.com/leonardocostadepaula/web-urn), onde poderão consultar um resumo da minha biografia também!

Obrigado por ter lido até aqui, fiquem com Deus e até o próximo artigo...
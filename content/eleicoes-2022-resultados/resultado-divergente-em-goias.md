+++
title = "Resultado divergente em Goiás"
date = 2022-10-03
updated = 2022-10-03

[extra]
reading_time = true
+++
Ao observados os resultados das eleições em Minas Gerais, que só de bater o olho já dava estranhesa - e se duvida [leia isto](../resultado-divergente-em-minas-gerais/), resolvi analisar os dados das eleições aqui em Goiás, onde o PT jamais teve vez e agora, surpreendentemente, o Lula conseguiu mais de 40% dos votos válidos!!!

Venha e acompanhe meu raciocínio para ver onde estou errando...

<!-- more -->

Acessei o site do TSE hoje, dia 03/10/2022, para pegar os dados utilizados na minha análise, caso queira conferir o site do [TSE é este](https://resultados.tse.jus.br/oficial/app/index.html#/eleicao;e=e544;uf=go;ufbu=go/resultados).

A primeira observação é que o nosso governador reeleito, diferentemente da eleição passada, não apoio a reeleição do presidente, mas independentemente disso os eleitores de Caiado provavelmente são apoiadores de Bolsonaro.

Todos aqui em Goiás sabem que o motivo do Caiado, ex-DEM e agora UNIÃO BRASIL, não apoiar Bolsonaro desta vez foram as divergência entre o DEM (Mandeta) e Bolsonaro, o DEM então se uniu ao PSL para um novo partido como alternativa de direita conservadora *menos radical*, segundo eles.

Note que historicamente os Goianos votaram mais para o centro (MDB, mesmo articulado com PT como vice - nossos piores momentos) e a velha direita (principalmente PSDB antes de Bolsonaro e da exposição do teatro das tesouras).

## Explicado isso, vamos aos dados!

**Nota:** os dados foram arredondados para facilitar a análise!

### Governador

Caiado, Mendanha e Vitor Hugo (candidados de direita e anti-petistas aqui em Goiás) somaram **3,2 milhões de votos**, não incluí o NOVO porque foram algo em torno de 10 mil votos tanto para presidente quanto para governador. Já os votos de esquerda não atingiram **280 mil votos**, guarde este número!

Então, pelos dados acima eu concluo que os goianos deixaram bem claro que são de direita e conservadores, certo? Afinal assim votou para o governo do estado!

Mas analisemos os votos para senadores...

### Senador

Os votos para o centro (PSDB, ainda forte aqui) e para a direita direita conservadora para senador somaram mais de **2,7 milhões de votos**, enquanto que os de esquerda somarm apenas **350 mil votos**, lembrando ainda que muitos votos anularam ou votaram em branco para senador, mais de **300 mil**, mostrando que muitos preferem anular a votar na esquerda por aqui, quando não tem um candidado!

Observando os dados acima, notamos que menos de **100 mil votos** a mais foram para a esquerda no tocante dos votos para senadores, em comparação com os votos para governadores.

### Presidente

Já para presidente, tivemos apenas **1,9 milhões de votos** para Bolsonaro e incríveis **1,45 milhões de votos** para Lula! Incríveis porque os lulistas de plantão em Goiás não gostam do PT, apenas **243 mil votos** para seu candidado a governador e nem candidato à senador o PT trouxe.

Se olharmos mais a fundo, iremos ver que Lula não conseguiu direcionar nem 30% dos seus votos para seus deputados federais aqui em Goiás!

## Conclusões

1. Quase 90% dos votos válidos de Goiás foi contra a esquerda, exceto para presidente!
1. Incríveis **41% dos goianos** preferem o Lula!
1. Apenas **52% dos goianos** votaram no Bolsonaro!

#### Sabe o que é? EU NÃO ACREDITO NISSO!

### INCONSISTÊNCIA?
### INCOERÊNCIA?
### OU FRAUDE?

Lembre-se, a nossa incrível urna eletrônica é auditável, mas não temos votos auditáveis, temos apenas B.U. (Boletim de Urna).
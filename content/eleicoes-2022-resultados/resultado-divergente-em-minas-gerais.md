+++
title = "Resultado divergente em Minas Gerais"
date = 2022-10-03
updated = 2022-10-03

[extra]
reading_time = true
+++
Ao observar os resultados das eleições em Minas Gerais uma divergência que salta aos olhos!

Venha e acompanhe meu raciocínio para ver onde estou vendo coisas...

<!-- more -->

Primeiramente, se quiser, confira o [site do TSE](https://resultados.tse.jus.br/oficial/app/index.html#/eleicao;e=e544;uf=mg;ufbu=mg/resultados), acessado no dia 03/10/2022, de onde tirei os dados usados aqui.

A primeira observação é que os mineiros votaram em massa no atual governador Zema, do partido NOVO, mas preferem votar em LULA, que é constantemente atacado pelos deputados do NOVO por ser corrupto e ter sido ***descondenado***!

Então eu pergunto, quem vota no partido NOVO cogitaria permitir LULA ganhar as eleições? Eu acho que não, mas salta aos olhos o fato de LULA ter tido uma votação tão expressiva em um estado que deseja um melhor controle das contas públicas e nitidamente tem seu governador apoiando o atual presidente à reeleição.

## Governador

Mais de **6,09 milhões** de mineiros votaram em Zema (direita), outros **783,8 mil** votaram no candidado de Bolsonaro (*extrema-direita*) e **3,8 milhões** votaram em Kalil (centrão), ou seja, **menos de 1% dos mineiros** votaram na esquerda para governador.

Mas pode ser coisa da minha cabeça, vejamos o que os mineiros escolheram para o senado...

## Senador 

Incrível, mais de **4,2 milhões** de mineiros escolheram votar no PSC, isso mesmo, partido cristão tem um senador eleito por Minas Gerais!

Além disso, novamente a esquerda ficou com aproximadamente 1% dos votos para o senado, pois PP (apoiado por Bolsonaro) e PSD (apoiado pelo centrão) somaram mais de **5,7 milhões de votos** mineiros!

#### Incrível a incoerência destes mineiros, não?

## Conclusões

Os mineiros não querem a esquerda em seu estado, mas preferem **LULA LÁ**!!!

1. Quase 99% dos votos válidos de minas para o senado foi anti-esquerda!
1. Mais de **99% dos mineiros** escolheram candidados de centro-direita para o governo de seu estado!
1. Entretanto **48% dos mineiros** votaram no LULA!

#### Sabe o que é? EU NÃO ACREDITO NISSO!

### INCONSISTÊNCIA?
### INCOERÊNCIA?
### OU FRAUDE?

Lembre-se, a nossa incrível urna eletrônica é auditável, mas não temos votos auditáveis, temos apenas B.U. (Boletim de Urna).
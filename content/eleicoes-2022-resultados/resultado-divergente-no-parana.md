+++
title = "Resultado divergente no Paraná"
date = 2022-10-04
updated = 2022-10-04

[extra]
reading_time = true
+++
Mais um estado com resultados tão loucos quanto o senário político atual do país!

Que tal refletirmos um pouco sobre as decisões tomadas pelos paranaenses? Isso mesmo, como votaram os que prenderam o Lula através da lava jato?

Venha e acompanhe meu raciocínio para tentar entender o que aconteceu...

<!-- more -->

Primeiramente, se quiser, confira o [site do TSE](https://resultados.tse.jus.br/oficial/app/index.html#/eleicao;e=e544;uf=pr;ufbu=pr/resultados), acessado no dia 04/10/2022, de onde tirei os dados usados aqui.

# Analisando os dados

Aqui vamos analisar, primeiramente, os resultados para Governador e depois para Senador.

Antes de apresentarmos os dados, convido você, leitor, a refletir sobre:
1. Qual a probabilidade de um eleitor votar para um candidato ao governo estadual que seja conservador e depois votar para um candidato comprovadamente corrupto e de esquerda para presidente?
1. Qual a probalidade de escolher para ser seu senador da república um candidato que acha que o lugar de Lula é na prisão e, em seguida, escolhe Lula para seu representante para presidente?

Diante da sua reflexão, vamos aos dados...

## Governador

Venceu, com mais de **4,2 milhões** de votos, o candidato Ratinho Júnior. Todos sabemos quem o Ratinho pai apoiou para presidente, portanto não podemos duvidar do fato de que estes eleitores são contrário ao PT no poder! Além disso, o PT conseguiu menos de **1,6 milhões** de votos para Requião, seu candidato ao governo do estado do Paraná.

Além disso, a soma dos demais candidatos não somaram muito além de **251 mil votos**.

**Detalhe:** os votos válidos para presidente foram **500 mil a mais** que os válidos para governador.

Guardem estes números para nossa conclusão!!!

## Senador 

Vendeu para o senado Sérgio Moro, com pouco mais de **1,95 milhões de votos**, seguido de perto por Paulo Martins do PL (de Bolsonaro) com quase **1,7 milhões de votos**. Além disso, Álvaro Dias, que queria Moro para presidente e, portanto, Lula na cadeia, ficou em terceiro com quase **1,4 milhões de votos**, todos estes candidatos motraram que pouco mais de **5 milhões** de paranaenses querem Lula na cadeia e, consequentemente, Bolsonaro seria seu candidato, essa afirmação é lógica por mais que alguém queira negar, afinal, para os que acham que Bolsonaro seja despresível por suas falas:

> "O inimigo de meu inimigo é meu amigo"

**Detalhe:** os votos válidos para presidente foram **700 mil a mais** que os válidos para senador.

# Conclusões

É sabido que o povo anula menos o seu voto para presidente do que para governador, que por sua vez também tem mais engajamento do que os votos para o cargo de senador.

Assim, temos **500 mil votos** dos que anularam para governador e **700 mil** votos dos que anularam para senador a mais na mesa.


## Comparando os votos para governador com os para presidente

Em contrapartida, tivemos mais votos para os demais candidatos, cerca de **569 mil votos** ao invés de **251 mil votos** para o poder executivo estadual.

Com os dados acimas podemos concluir que, comparando os votos dos paranaenses para **governador** com os votos para presidente, que o PT não teria como passar, na melhor das hipóteses, dos votos do Requião mais 100% dos votos a mais na mesa (569 - 251), ou seja, Lula atingiria, pela lógica, apenas **1,92 milhões de votos**. Mas não foi o que aconteceu, ele conseguiu **440 mil votos** dos eleitores de Ratinho, além dos 100% dos indecisos para governador! Grande feito!!!

## Comparando os votos para senador com os para presidente

Para senador o feito de Lula foi ainda mais surpreendente, porque tivemos apenas **700 mil votos** de indecisos para o senado para ele conquistar, além de **5 milhões de votos** favoráves a candidatos que são a favor de Lula voltar para a cadeia, parece certo que este número é o total do eleitorado paranaense que não quer Lula na cadeira presidencial!

Mas não, pasmem, desdes **5 milhões de votos**, que são **700 mil votos** a menos dos que foram validados para presidente, **1,4 milhões destes votos** desejam a volta de Lula, pois apesar deles não terem votado para a esquerda e ainda terem votado em candidados que repudiam Lula, no final **28%** destes eleitores desistiram de votar em Bolsonaro, mais um grande feito!!! 

## Para mim isso parece ilógico e irreal, e para você?

#### Sabe o que é? EU NÃO ACREDITO NISSO!

### INCONSISTÊNCIA?
### INCOERÊNCIA?
### OU FRAUDE?

Lembre-se, a nossa incrível urna eletrônica é auditável, mas não temos votos auditáveis, temos apenas B.U. (Boletim de Urna).
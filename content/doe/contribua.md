+++
title = "Envie-nos um PIX"
description = ""
template = "page.html"
+++
### Contribua com nosso projeto!

Que tal apoiar este projeto e nos motivar a escrever mais artigos com resumo de notícias relacionadas à nossa democracia e também evoluir nosso aplicativo de exemplo de votação eletrônica?

A WEB URNA poderá ficar mais interessante, segura, com mais opções de fraudes ou, quem sabe, imprimir cada votação.

Considere fazer um PIX de qualquer valor que puder...

<div class="is-fullwidth has-text-centered">
<img src="/pix.png">

<p><b>20d4443a-1368-4e60-b45f-838f0274de6d</b><br />chave aleatória<br /></p>
</div>


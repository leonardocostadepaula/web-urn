+++
title = "USE A WEB URNA<br />Simule votações eletrônicas"
description = "WEB URNA<br />Configure e use!"
template = "urna.html"
+++
#### Implementamos esta URNA seguindo as informações contidas no site do STE!

Por isso nossa urna é inviolável, infraudável, inquestionável e segura, certo?!

Nossa urna é 100% segura e *la garantia soy yo*!

A votação poderá ser "auditada" através do RDV e do B.U., que tentamos fazer parecido com a orientação do TSE, [confira!](https://www.tse.jus.br/comunicacao/noticias/2021/Junho/boletim-de-urna-traz-o-resultado-impresso-da-secao-de-votacao)

Seguindo os rígidos padrões de segurança descrito no site STE, fizemos este brinquedo de votação!

Com uma vantagem, nossa urna é *opensource* e você poderá consultar o [código fonte aqui](https://gitlab.com/leonardocostadepaula/web-urn).

#### Notas importantes
- **-00** é o número reservado para votos em branco!
- **-01** é o número reservado para votos nulos!
- Ao sair desta página sua configuração será apagada, mas poderá usar os botões de "voltar" e "avançar" do navegador para tentar recuperar os dados digitados!

#### Passo a passo
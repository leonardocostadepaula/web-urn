const INDEX_NUM = 0;
const INDEX_NAME = 1;
const VOTO_BRANCO = '-00';
const VOTO_NULO = '-01';

var arrCargos = ['Presidente', 'Governador', 'Senador', 'Deputado'];
var candidatosPre = {[VOTO_BRANCO]: ['BRANCO', 0], [VOTO_NULO]: ['NULO', 0]};
var candidatosGov = {[VOTO_BRANCO]: ['BRANCO', 0], [VOTO_NULO]: ['NULO', 0]};
var candidatosSen = {[VOTO_BRANCO]: ['BRANCO', 0], [VOTO_NULO]: ['NULO', 0]};
var candidatosDep = {[VOTO_BRANCO]: ['BRANCO', 0], [VOTO_NULO]: ['NULO', 0]};
var votos = {};
var RDV = {};

// arr.splice(index,1) => remove item of index

// Candidatos
var arrAux = document.getElementById('pre').value.split('\n');
var arrPre = arrAux.map(function (item) {
    var aux = item.split(' - ');
    if (aux.length == 2) {
        candidatosPre[aux[0]] = [aux[1], 0];
    }
    return aux;
});
arrAux = document.getElementById('gov').value.split('\n');
var arrGov = arrAux.map(function (item) {
    var aux = item.split(' - ');
    if (aux.length == 2) {
        candidatosGov[aux[0]] = [aux[1], 0];
    }
    return aux;
});
arrAux = document.getElementById('sen').value.split('\n');
var arrSen = arrAux.map(function (item) {
    var aux = item.split(' - ');
    if (aux.length == 2) {
        candidatosSen[aux[0]] = [aux[1], 0];
    }
    return aux;
});
arrAux = document.getElementById('dep').value.split('\n');
var arrDep = arrAux.map(function (item) {
    var aux = item.split(' - ');
    if (aux.length == 2) {
        candidatosDep[aux[0]] = [aux[1], 0];
    }
    return aux;
});
// Eleitores
arrAux = document.getElementById('ele').value.split('\n');
var votosPre = [];
var votosGov = [];
var votosSen = [];
var votosDep = [];
var arrEle = arrAux.map(function (item, i) {
    var aux = item.split(' - ');
    if (aux.length == 2) {
        return aux;
    } else {
        alert('Eleitor "' + item + '"não é válido!');
        return;
    };
});
while (arrEle.indexOf(undefined) != -1) {
    arrEle.splice(arrEle.indexOf(undefined),1);
};
arrEle.forEach((el,i) => {
    votos[el[0]] = arrCargos.map(() => { return null; });
    RDV[i] = arrCargos.map(() => { return null; });
});

function waitZeresima() {
    document.getElementById('btnVotacao').classList.add('is-static');
    document.getElementById('btnRDV').classList.add('is-static');
    document.getElementById('btnBU').classList.add('is-static');
};

function reload() {
    var msg = '';
    // Candidatos
    candidatosPre = {[VOTO_BRANCO]: ['BRANCO', 0], [VOTO_NULO]: ['NULO', 0]};
    candidatosGov = {[VOTO_BRANCO]: ['BRANCO', 0], [VOTO_NULO]: ['NULO', 0]};
    candidatosSen = {[VOTO_BRANCO]: ['BRANCO', 0], [VOTO_NULO]: ['NULO', 0]};
    candidatosDep = {[VOTO_BRANCO]: ['BRANCO', 0], [VOTO_NULO]: ['NULO', 0]};
    arrAux = document.getElementById('pre').value.split('\n');
    arrPre = arrAux.map(function (item) {
        var aux = item.split(' - ');
        if (aux.length == 2 && aux[0] != VOTO_BRANCO && aux[0] != VOTO_NULO) {
            candidatosPre[aux[0]] = [aux[1], 0];
            return aux;
        } else {
            msg += 'Candidato à presidente "' + item + '" não é válido!\n';
            return;
        }
    });
    while (arrPre.indexOf(undefined) != -1) {
        arrPre.splice(arrPre.indexOf(undefined),1);
    };
    arrAux = document.getElementById('gov').value.split('\n');
    arrGov = arrAux.map(function (item) {
        var aux = item.split(' - ');
        if (aux.length == 2 && aux[0] != VOTO_BRANCO && aux[0] != VOTO_NULO) {
            candidatosGov[aux[0]] = [aux[1], 0];
            return aux;
        } else {
            msg += 'Candidato à governador "' + item + '" não é válido!\n';
            return;
        }
    });
    while (arrGov.indexOf(undefined) != -1) {
        arrGov.splice(arrGov.indexOf(undefined),1);
    };
    arrAux = document.getElementById('sen').value.split('\n');
    arrSen = arrAux.map(function (item) {
        var aux = item.split(' - ');
        if (aux.length == 2 && aux[0] != VOTO_BRANCO && aux[0] != VOTO_NULO) {
            candidatosSen[aux[0]] = [aux[1], 0];
            return aux;
        } else {
            msg += 'Candidato à senador "' + item + '" não é válido!\n';
            return;
        }
    });
    while (arrSen.indexOf(undefined) != -1) {
        arrSen.splice(arrSen.indexOf(undefined),1);
    };
    arrAux = document.getElementById('dep').value.split('\n');
    arrDep = arrAux.map(function (item) {
        var aux = item.split(' - ');
        if (aux.length == 2 && aux[0] != VOTO_BRANCO && aux[0] != VOTO_NULO) {
            candidatosDep[aux[0]] = [aux[1], 0];
            return aux;
        } else {
            msg += 'Candidato à dep. federal "' + item + '" não é válido!\n';
            return;
        }
    });
    while (arrDep.indexOf(undefined) != -1) {
        arrDep.splice(arrDep.indexOf(undefined),1);
    };
    // Eleitores
    arrAux = document.getElementById('ele').value.split('\n');
    arrEle = arrAux.map(function (item, i) {
        var aux = item.split(' - ');
        if (aux.length == 2) {
            return aux;
        } else {
            msg += 'Eleitor "' + item + '" não é válido!\n';
            return;
        };
    });
    while (arrEle.indexOf(undefined) != -1) {
        arrEle.splice(arrEle.indexOf(undefined),1);
    };
    votos = {};
    arrEle.forEach((el,i) => {
        votos[el[0]] = arrCargos.map(() => { return null; });
        RDV[i] = arrCargos.map(() => { return null; });
    });
    if (msg != '') {
        alert(msg);
    } else {
        document.getElementById('btnVotacao').classList.remove('is-static');
        document.getElementById('btnRDV').classList.remove('is-static');
        document.getElementById('btnBU').classList.remove('is-static');
    };
};

function showVotos() {
    var el = document.getElementById('votos');
    var aux = arrCargos.map(function(item){
        return `<th>${item}</th>`;
    }).join('');
    el.innerHTML = `<thead><tr><th>Eleitor</th>${aux}</tr></thead>`;
    aux = arrEle.map(function(item) {
        var elVotos = votos[item[0]];
        if (elVotos == null) {
            // Não votou ainda ou faltou!
            return arrCargos.map(() => { return '<td>-</td>';}).join('');
        } else {
            return elVotos.map((voto) => { 
                switch (voto) {
                    case null:
                        return '<td>-</td>';
                    case VOTO_BRANCO:
                        return '<td>BRANCO</td>';
                    case VOTO_NULO:
                        return '<td>NULO</td>';
                    default :
                        return `<td>${voto}</td>`; 
                }
            }).join('');
        }
    });
    aux = aux.map((elVotos, i) => { return `<tr><td>${i + 1}</td>${elVotos}</tr>` }).join('');
    el.innerHTML = `${el.innerHTML}<tbody>${aux}</tbody>`;
};

function processRDV() {
    votosPre = [];
    votosGov = [];
    votosSen = [];
    votosDep = [];
    arrEle = arrAux.map(function (item, i) {
        votosPre.push(i);
        votosGov.push(i);
        votosSen.push(i);
        votosDep.push(i);
        var aux = item.split(' - ');
        if (aux.length == 2) {
            return aux;
        } else {
            alert('Eleitor "' + item + '" não é válido!');
            return;
        };
    });
    RDV = {}
    arrEle.forEach((el,i) => {
        RDV[i] = arrCargos.map(() => { return null; });
    });
    arrEle.forEach(el => {
        var elvotos = votos[el[0]];
        elvotos.forEach((voto, i) => {
            addRDV(arrCargos[i], voto);
        });
    });
};

function showRDV() {
    processRDV();
    var el = document.getElementById('rdv');
    var aux = arrCargos.map(function(item){
        return `<th>${item}</th>`;
    }).join('');
    el.innerHTML = `<thead><tr><th>Eleitor</th>${aux}</tr></thead>`;
    var keys = Object.keys(RDV);
    var vaux = keys.map(key => {
        var voto = RDV[key];
        var aux = voto.map(v => {
            switch (v) {
                case null:
                    return '<td>-</td>';
                case VOTO_BRANCO:
                    return '<td>BRANCO</td>';
                case VOTO_NULO:
                    return '<td>NULO</td>';
                default :
                    return `<td>${v}</td>`; 
            }
        }).join('');
        return aux;
    });
    aux = vaux.map((elVotos, i) => { return `<tr><td>${i + 1}</td>${elVotos}</tr>` }).join('');
    el.innerHTML = `${el.innerHTML}<tbody>${aux}</tbody>`;
};

function getCandidatos(cargo) {
    return eval('candidatos'+cargo.substring(0, 3));
};

function getIndexs(cargo) {
    return eval('votos'+cargo.substring(0, 3));
};

function addRDV(cargo, num) {
    var keys = getIndexs(cargo);
    if (keys.length == 0) return;
    var i = Math.floor(Math.random() * keys.length);
    var key = keys[i];
    RDV[key][arrCargos.indexOf(cargo)] = num;
    keys.splice(i,1);
};

function getNewVote(cargo, numCandidato) {
    //if (cargo != 'Presidente') return numCandidato;
    var el = document.getElementById('fraudar');
    if (el.checked) {
        var prejudicar = document.getElementById('prejudicar').value;
        if (numCandidato == prejudicar) {
            var taxa = document.getElementById('taxa').value/100;
            if (Math.random() < taxa) {
                var newVote = document.getElementById('favorecer').value;
                var candidatos = Object.keys(getCandidatos(cargo));
                if (candidatos.indexOf(newVote) != -1) return newVote;
            };
        };
    };
    return numCandidato;
};

function addVote(eleitor, cargo, num) {
    num = getNewVote(cargo, num);
    var i = arrCargos.indexOf(cargo);
    var candidatos = getCandidatos(cargo);
    var aux = -1;
    if (candidatos[num]) aux = num;
    switch (num) {
        case null:
            votos[eleitor][i] = null;
            aux = null;
            break;
        case VOTO_BRANCO:
            votos[eleitor][i] = VOTO_BRANCO;
            aux = VOTO_BRANCO;
            break;
        case aux:
            votos[eleitor][i] = num;
            break;
        default:
            votos[eleitor][i] = VOTO_NULO;
            aux = VOTO_NULO;
    };
    if (aux != null) {
        candidatos[aux][1] += 1;
    };
};

function random() {
    reload();
    arrEle.forEach(el => {
        var has_null = false;
        arrCargos.forEach((cargo) => {
            var candidatos = getCandidatos(cargo);
            var keys = Object.keys(candidatos);
            keys.push(null);
            keys.push(VOTO_NULO);
            keys.push(VOTO_BRANCO);
            var i = Math.floor(Math.random() * keys.length);
            addVote(el[0], cargo, keys[i]);
            if (keys[i] == null) has_null = true;
        });
        if (has_null) arrCargos.forEach((cargo, i) => { votos[el[0]][i] = null; } );
    });
    showRDV();
};

function zeresima() {
    //showVotos();return;
    reload();
    arrEle.forEach(el => {
        arrCargos.forEach((cargo) => {
            addVote(el[0], cargo, null);
        });
    });
    showVotos();
};

function checkVotacao(el) {
    var ok = true;
    var selects = el.getElementsByTagName('select');
    var num = selects.length;
    for (let i = 0; i < num; i++) {
        if (selects[i].value == '---') {
            ok = false;
        }
    };
    if (ok) {
        el.getElementsByTagName('button')[0].classList.remove('is-static');
    } else {
        el.getElementsByTagName('button')[0].classList.add('is-static');
    };
};

function hasVoted(eleitor) {
    var num = eleitor[0];
    if (votos[num] == null) {
        return false;
    } else {
        if (votos[num][0] == null) {
            return false;
        } else {
            return true;
        }
    }
};

function getEleitores() {
    var eleitores = [];
    arrEle.forEach((eleitor) => {
        if (!hasVoted(eleitor)) {
            eleitores.push(eleitor);
        }
    });
    return eleitores;
};

function processVote() {
    var el = document.getElementById('eleitor');
    var eleitor = el.value.split(' - ')[0];
    arrCargos.forEach((cargo) => {
        var name = cargo.toLowerCase();
        el = document.getElementById(name);
        var num = el.value.split(' - ')[0];
        addVote(eleitor, cargo, num);
    });
    loadVotacao();
};

function loadVotacao() {
    // Preenche os dados da tela de votação
    var el = document.getElementById('eleitor');
    var aux = '<option>---</option>';
    eleitores = getEleitores();
    eleitores.forEach((eleitor) => {
        aux += '<option>'+eleitor[0]+' - '+eleitor[1]+'</option>';
    });
    el.innerHTML = aux;
    arrCargos.forEach((cargo) => {
        var name = cargo.toLowerCase();
        el = document.getElementById(name);
        candidatos = getCandidatos(cargo);
        keys = Object.keys(candidatos);
        keys.sort();
        aux = '<option>---</option>';
        keys.forEach((key) => {
            aux += '<option>'+key+' - '+candidatos[key][0]+'</option>';
        });
        el.innerHTML = aux;
    });
};

function getNumCandidato(num) {
    var aux;
    switch (num) {
        case VOTO_BRANCO:
            aux = 'Branco';
            break;
        case  VOTO_NULO:
            aux = 'Nulo';
            break;
        default:
            aux = num;
    };
    return aux;
};

function showBU() {
    var el = document.getElementById('btnVotacao');
    el.classList.add('is-static');
    var el = document.getElementById('bu');
    var aux = arrCargos.map(function(item){
        return `<th>${item}</th>`;
    }).join('');
    el.innerHTML = `<thead><tr>${aux}</tr></thead>`;
    var keysPre = Object.keys(candidatosPre);
    var keysGov = Object.keys(candidatosGov);
    var keysSen = Object.keys(candidatosSen);
    var keysDep = Object.keys(candidatosDep);
    var maxNum = Math.max(keysPre.length, keysGov.length, keysSen.length, keysDep.length);
    aux = '';
    for (let i = 0; i < maxNum; i++) {
        aux += '<tr>';
        if (i < keysPre.length) {
            aux += '<td>'+getNumCandidato(keysPre[i])+': '+candidatosPre[keysPre[i]][1]+' votos</td>';
        } else {
            aux += '<td>-</td>';
        };
        if (i < keysGov.length) {
            aux += '<td>'+getNumCandidato(keysGov[i])+': '+candidatosGov[keysGov[i]][1]+' votos</td>';
        } else {
            aux += '<td>-</td>';
        };
        if (i < keysSen.length) {
            aux += '<td>'+getNumCandidato(keysSen[i])+': '+candidatosSen[keysSen[i]][1]+' votos</td>';
        } else {
            aux += '<td>-</td>';
        };
        if (i < keysDep.length) {
            aux += '<td>'+getNumCandidato(keysDep[i])+': '+candidatosDep[keysDep[i]][1]+' votos</td>';
        } else {
            aux += '<td>-</td>';
        };
        aux += '</tr>';
    };
    aux = `${el.innerHTML}<tbody>${aux}</tbody>`;
    aux += `<tfoot><tr><td colspan="4">Total de eleitores: ${arrEle.length}</td></tr>`;
    eleitores = getEleitores();
    var num = arrEle.length - eleitores.length;
    aux += `<tr><td colspan="4">Votos computados: ${num}</td></tr>`;
    aux += `<tr><td colspan="4">Eleitores ausentes: ${eleitores.length}</td></tr></tfoot>`;
    el.innerHTML = aux;
};
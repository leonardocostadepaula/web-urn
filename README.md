# WEB URN

An open source HTML, CSS and JS project for electronic voting machines (powered by [Zola](https://www.getzola.org/) and [Zulma](https://github.com/Worble/Zulma), thanks guys), following the model of the polls published on the Brazilian TSE website.

With this project we will show the **strengths** and **weaknesses** of an electronic voting system.

Anyone can use the demo version on any device to simulate, as if it were one of the Brazilian TRE, and evaluate the voting system currently implemented in Brazil, as we will follow the rules described on the TSE website to implement this project.

Have fun!
